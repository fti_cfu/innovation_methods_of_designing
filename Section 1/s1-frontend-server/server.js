const express = require('express');
const path = require('path');
const app = express();
const config = require('./config.json');


app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.listen(config.port, function () {
  console.log('Server listening on port ' + config.port);
});