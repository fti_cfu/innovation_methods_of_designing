const express = require('express');
const path = require('path');
const app = express();
const config = require('./config.json');

app.use(express.static('public'));

app.listen(config.port, function () {
  console.log('Server listening on port ' + config.port);
});