const express = require('express');
const app = express();
const config = require('./config.json');

app.get('/', function (req, res) {
  res.json({
    name: 'backend',
    status: 'working'
  });
});

app.listen(config.port, function () {
  console.log('Server listening on port ' + config.port);
});