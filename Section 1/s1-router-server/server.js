const express = require('express');
const axios = require('axios');
const app = express();
const config = require('./config.json');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

for (const key in config.routes) {
  const route = (key === 'index' ? '/' : new RegExp('/'+key+'/.*$'));
  console.log(route);
  app.get(route, function (req, res) {
    const proxy = 'http://127.0.0.1:' + config.routes[key] + req.originalUrl.replace('/'+key, '');
    console.log(proxy);
    axios.get(proxy)
      .then(function (resp) {
        console.log('Retrieve response');
        for (const header in resp.headers) {
          res.header(header, resp.headers[header]);
        }
        res.send(resp.data);
      })
      .catch(function (e) {
        console.error(e);
        res.send('error');
      });
  });
}

app.listen(config.port, function () {
  console.log('Server listening on port ' + config.port);
});